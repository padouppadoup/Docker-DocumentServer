Building a chifr/ed version of Onlyoffice 
-----------------------------------------

https://github.com/ONLYOFFICE/Docker-DocumentServer/blob/master/Dockerfile

```{shell}
git clone https://gitlab.com/padouppadoup/Docker-DocumentServer.git
cd Docker-DocumentServer

## In the following, replace key1 by the actual secret key
docker build -t onlyoffice-chifr --build-arg SECRET_KEY=key_pw . # Takes some time

```

Running:

```{shell}
docker run -i -t -d -p 8000:80 --restart=always \
	-v /app/onlyoffice/DocumentServer/logs:/var/log/onlyoffice \
    -v /app/onlyoffice/DocumentServer/data:/var/www/onlyoffice/Data \
    -v /app/onlyoffice/DocumentServer/lib:/var/lib/onlyoffice \
    -v /app/onlyoffice/DocumentServer/db:/var/lib/postgresql \
	onlyoffice-chifr
```
