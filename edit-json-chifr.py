## Simple script to edit the onlyoffice default.json configuration file
## By the chifr team, Mar. 2018

## This script does an in-place edition of the script given as the first
## argument of the script.

import sys, json

path = sys.argv[1]
secret_inbox = sys.argv[2]
secret_outbox = sys.argv[3]

data  = json.loads(open(path).read())

#print(json['services']['CoAuthoring']['secret']['inbox']['string'])
#print(json['services']['CoAuthoring']['secret']['outbox']['string'])
#print(json['services']['CoAuthoring']['token']['enable']['browser'])
#print(json['services']['CoAuthoring']['token']['enable']['request']['inbox'])
#print(json['services']['CoAuthoring']['token']['enable']['request']['outbox'])

data['services']['CoAuthoring']['secret']['inbox']['string'] = secret_inbox
data['services']['CoAuthoring']['secret']['outbox']['string'] = secret_outbox
data['services']['CoAuthoring']['token']['enable']['browser'] = True
data['services']['CoAuthoring']['token']['enable']['request']['inbox'] = True
data['services']['CoAuthoring']['token']['enable']['request']['outbox'] = True

with open(path, 'w') as outfile:
    json.dump(data, outfile, indent=4, sort_keys=True)
